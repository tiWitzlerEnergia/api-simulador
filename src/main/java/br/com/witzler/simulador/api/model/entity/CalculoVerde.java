package br.com.witzler.simulador.api.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table( name="calculoVerde")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CalculoVerde {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="id_unidade")
	private Integer id_unidade;
	
	@Column(name="id_empresa")
	private Integer id_empresa;
	
	@Column(name="ano")
	private Integer ano;
	
	@Column(name="mensal")
	private Double mensal;
	
	@Column(name="anual")
	private Double anual;

}
