package br.com.witzler.simulador.api.model.rest.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.witzler.simulador.api.dto.ResultadosDTO;
import br.com.witzler.simulador.api.model.entity.Resultados;
import br.com.witzler.simulador.api.model.repository.ResultadosRepository;

@RestController
@RequestMapping("/resultados/")
public class ResultadosController {
	
	private final ResultadosRepository repository;
	
	@Autowired
	public ResultadosController( ResultadosRepository repository ) {
		this.repository = repository;
	}
	
	@GetMapping( "consulta/{id_empresa}" )
	ArrayList<ResultadosDTO> listarResultados( @PathVariable( "id_empresa" ) Integer id_empresa ) {
		
		List<Resultados> resultados = repository.listarResultadosPorEmpresa(id_empresa);
		
		ArrayList<ResultadosDTO> ConjuntoResultados = new ArrayList<>();
		for ( int i=0; i<resultados.size(); i++ )
		{
			ResultadosDTO r = new ResultadosDTO();
			r.setId(resultados.get(i).getId());
			r.setId_empresa(resultados.get(i).getId_empresa());
			r.setId_unidade(resultados.get(i).getId_unidade());
			r.setTipo_tarifa(resultados.get(i).getTipo_tarifa());
			r.setAno(resultados.get(i).getAno());
			r.setMensal(resultados.get(i).getMensal());
			r.setAnual(resultados.get(i).getAnual());
			r.setPorcentagem(resultados.get(i).getPorcentagem());
			
			ConjuntoResultados.add(r);
		}
		
		return ConjuntoResultados;
		
	}
	
	
	@GetMapping( "consulta/{id_empresa}/{tipo_tarifa}" )
	ArrayList<ResultadosDTO> listarResultadosPorTarifa( @PathVariable( "id_empresa" ) Integer id_empresa, @PathVariable( "tipo_tarifa" ) String tipo_tarifa ) {
		
		List<Resultados> resultados = repository.listarResultadosPorTarifa(id_empresa, tipo_tarifa);
		
		
		//System.out.println(resultados.size()); <- Aqui ele mostra a quantidade elementos armazenados na vari�vel resultados
		
		ArrayList<ResultadosDTO> ConjuntoResultados = new ArrayList<>();
		//ArrayList<ResultadosDTO> lista = new ArrayList<>(); // Criando uma segunda lsita que tambem vai receber os valores e ser�o comparados para excluir os repetidos
		for ( int i=0; i<resultados.size(); i++ )
		{
			ResultadosDTO r = new ResultadosDTO();
			r.setId(resultados.get(i).getId());
			r.setId_empresa(resultados.get(i).getId_empresa());
			r.setId_unidade(resultados.get(i).getId_unidade());
			r.setTipo_tarifa(resultados.get(i).getTipo_tarifa());
			r.setAno(resultados.get(i).getAno());
			r.setMensal(resultados.get(i).getMensal());
			r.setAnual(resultados.get(i).getAnual());
			r.setPorcentagem(resultados.get(i).getPorcentagem());
			
			ConjuntoResultados.add(r);
			//lista.add(r);
			
			//System.out.println(lista);

			
		}

		return ConjuntoResultados;
		
	}
	
	
	

}
