package br.com.witzler.simulador.api.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.witzler.simulador.api.model.entity.Resultados;
import br.com.witzler.simulador.api.model.entity.Unidades;

public interface ResultadosRepository extends JpaRepository<Resultados, Integer> {
	
	@Query(value="select * from resultados where id_empresa = :id_empresa", nativeQuery=true)
	List<Resultados> listarResultadosPorEmpresa( @Param ("id_empresa") Integer id_empresa );
	
	@Query(value="select * from resultados where id_empresa = ? and tipo_tarifa= ?", nativeQuery=true)
	List<Resultados> listarResultadosPorTarifa( @Param( "id_empresa" ) Integer id_empresa, @Param( "tipo_tarifa" ) String tipo_tarifa );

}
