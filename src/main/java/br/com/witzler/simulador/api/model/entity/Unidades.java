package br.com.witzler.simulador.api.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table( name="unidades")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Unidades {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_unidade;
	
	@Column(name="id_empresa")
	private Integer id_empresa;
	
	@Column(name="nome_unidade")
	private String nome_unidade;
	
	@Column(name="distribuidora")
	private Integer distribuidora;
	
	@Column(name="tipo_tarifa")
	private String tipo_tarifa;
	
	@Column(name="demanda_ponta_contratada")
	private double demanda_ponta_contratada;
	
	@Column(name="demanda_fora_ponta_unica_contratada")
	private double demanda_fora_ponta_unica_contratada;
	
	@Column(name="transformador")
	private Boolean transformador;
	
	@Column(name="consumo_ponta")
	private double consumo_ponta;
	
	@Column(name="consumo_fora_ponta")
	private double consumo_fora_ponta;
	
	@Column(name="demanda_ponta_registrada")
	private Double demanda_ponta_registrada;
	
	@Column(name="demanda_fora_ponta_registrada")
	private Double demanda_fora_ponta_registrada;
	
}
