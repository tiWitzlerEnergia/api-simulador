package br.com.witzler.simulador.api.model.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.witzler.simulador.api.model.repository.PrecoEnergiaRepository;
import br.com.witzler.simulador.api.dto.PrecoEnergiaDTO;
import br.com.witzler.simulador.api.model.entity.PrecoEnergia;

@RestController
@RequestMapping("/precoEnergia/")
public class PrecoEnergiaController {
	
	
	private final PrecoEnergiaRepository repository;
	
	@Autowired
	public PrecoEnergiaController( PrecoEnergiaRepository repository ) {
		this.repository = repository;
	}
	
	@GetMapping("consulta/{ano}")
	ArrayList<PrecoEnergiaDTO> listaPrecoEnergia( @PathVariable( "ano" ) Integer ano ) {
		
		List<PrecoEnergia> preco = repository.listarPrecoEnergia(ano);
		
		ArrayList<PrecoEnergiaDTO> ConjuntoPreco = new ArrayList<>();
		
		for( int i=0; i<preco.size(); i++ )
		{
			PrecoEnergiaDTO p = new PrecoEnergiaDTO();
			p.setId(preco.get(i).getId());
			p.setPreco(preco.get(i).getPreco());
			p.setAno(preco.get(i).getAno());
			
			ConjuntoPreco.add(p);
		}
		
		return ConjuntoPreco;
		
	}
	
	/*@GetMapping("consulta")
	public List<PrecoEnergia> listaPrecoEnergia() {
		return repository.findAll();
	}*/

}
