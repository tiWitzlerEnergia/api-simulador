package br.com.witzler.simulador.api.dto;

import br.com.witzler.simulador.api.model.entity.Empresa;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EspecialidadeDTO {

	private Integer id;
	private String especialidade;
	
}
