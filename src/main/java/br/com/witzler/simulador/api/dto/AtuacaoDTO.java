package br.com.witzler.simulador.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AtuacaoDTO {
	
	private Integer id;
	private String area_atuacao;

}
