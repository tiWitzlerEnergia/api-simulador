package br.com.witzler.simulador.api.model.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.witzler.simulador.api.model.repository.IcmsRepository;
import br.com.witzler.simulador.api.dto.IcmsDTO;
import br.com.witzler.simulador.api.model.entity.Icms;

@RestController
@RequestMapping("/icms/")
public class IcmsController {
	
	private final IcmsRepository repository;
	
	@Autowired
	public IcmsController( IcmsRepository repository ) {
		this.repository = repository;
	}
	
	
	@GetMapping("consulta/{id}")
	ArrayList<IcmsDTO> listaIcmsEstado( @PathVariable ("id") Integer id ) {
		
		List<Icms> icms = repository.listarIcmsPorIdEstado(id);
		
		ArrayList<IcmsDTO> ConjuntoIcms = new ArrayList<>();
		
		for( int i=0; i<icms.size(); i++)
		{
			IcmsDTO ic = new IcmsDTO();
			ic.setValor(icms.get(i).getValor());
			
			ConjuntoIcms.add(ic);
		}
		
		return ConjuntoIcms;
	}
	
	

}
