package br.com.witzler.simulador.api.model.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.witzler.simulador.api.model.repository.EspecialidadeRepository;
import br.com.witzler.simulador.api.model.entity.Especialidade;

@RestController
@RequestMapping("/especialidade/")
public class EspecialidadeController {
	
	@Autowired
	private EspecialidadeRepository especialidadeRepository;
	
	@GetMapping("consulta")
	public List<Especialidade> listaEspecialidade() {
		return especialidadeRepository.findAll();
	}

}
