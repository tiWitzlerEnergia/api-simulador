package br.com.witzler.simulador.api.dto;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TarifasDTO {

	private Integer id;
	private Integer id_distribuidora;
	private Integer subgrupo;
	private Double demanda_ponta;
	private Double demanda_fora_ponta;
	private Double consumo_ponta;
	private Double te_ponta;
	private Double te_fora_ponta;
	private Double ultrapassagem_ponta;
	private Double ultrapassagem_fora_ponta;
	private String tipo_tarifa;
	private Double consumo_fora_ponta;
	private Double reativo;
	
}
