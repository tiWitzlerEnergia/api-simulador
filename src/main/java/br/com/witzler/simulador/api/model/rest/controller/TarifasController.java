package br.com.witzler.simulador.api.model.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.witzler.simulador.api.dto.TarifasDTO;
import br.com.witzler.simulador.api.model.entity.Tarifas;
import br.com.witzler.simulador.api.model.repository.TarifasRepository;

@RestController
@RequestMapping("/tarifas/")
public class TarifasController {
	
	private final TarifasRepository repository;
	
	@Autowired
	public TarifasController( TarifasRepository repository ) {
		this.repository = repository;
	}
	
	@GetMapping( "consulta/{id_distribuidora}/{tipo_tarifa}" )
	ArrayList<TarifasDTO> listaTarifas( @PathVariable( "id_distribuidora" ) Integer id_distribuidora, @PathVariable( "tipo_tarifa" ) String tipo_tarifa ) {
		
		List<Tarifas> tarifas = repository.listarTarifasPorDistribuidora(id_distribuidora, tipo_tarifa);
		
		ArrayList<TarifasDTO> ConjuntoTarifas = new ArrayList<>();
		
		for( int i=0; i<tarifas.size(); i++ )
		{
			TarifasDTO t = new TarifasDTO();
			t.setId(tarifas.get(i).getId());
			t.setId_distribuidora(tarifas.get(i).getId_distribuidora());
			t.setDemanda_ponta(tarifas.get(i).getDemanda_ponta());
			t.setDemanda_fora_ponta(tarifas.get(i).getDemanda_fora_ponta());
			t.setConsumo_ponta(tarifas.get(i).getConsumo_ponta());
			t.setConsumo_fora_ponta(tarifas.get(i).getConsumo_fora_ponta());
			t.setTe_ponta(tarifas.get(i).getTe_ponta());
			t.setTe_fora_ponta(tarifas.get(i).getTe_fora_ponta());
			t.setUltrapassagem_ponta(tarifas.get(i).getUltrapassagem_ponta());
			t.setUltrapassagem_fora_ponta(tarifas.get(i).getUltrapassagem_fora_ponta());
			t.setTipo_tarifa(tarifas.get(i).getTipo_tarifa());
			t.setReativo(tarifas.get(i).getReativo());
			
			ConjuntoTarifas.add(t);
		}
		
		return ConjuntoTarifas;
		
	}

}
