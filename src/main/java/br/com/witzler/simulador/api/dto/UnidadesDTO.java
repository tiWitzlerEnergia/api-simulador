package br.com.witzler.simulador.api.dto;

import javax.persistence.Column;
import javax.persistence.ManyToOne;

import br.com.witzler.simulador.api.model.entity.Empresa;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UnidadesDTO {
	
	private Integer id_unidade;
	private Integer id_empresa;
	private String nome_unidade;
	private Integer distribuidora;
	private String tipo_tarifa;
	private double demanda_ponta_contratada;
	private double demanda_fora_ponta_unica_contratada;
	private boolean transformador;
	private double consumo_ponta;
	private double consumo_fora_ponta;
	private double demanda_ponta_registrada;
	private double demanda_fora_ponta_registrada;

}
