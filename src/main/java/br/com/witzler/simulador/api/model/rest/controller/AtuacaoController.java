package br.com.witzler.simulador.api.model.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.witzler.simulador.api.model.entity.Atuacao;
import br.com.witzler.simulador.api.model.repository.AtuacaoRepository;

@RestController
@RequestMapping("/atuacao/")
public class AtuacaoController {
	
	@Autowired
	private AtuacaoRepository atuacaoRepository;
	
	@GetMapping("consulta")
	public List<Atuacao> listaAtuacao() {
		return atuacaoRepository.findAll();
	}

}
