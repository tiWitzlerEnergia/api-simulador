package br.com.witzler.simulador.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiSimuladorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiSimuladorApplication.class, args);
	}

}
