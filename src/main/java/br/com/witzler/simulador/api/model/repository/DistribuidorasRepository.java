package br.com.witzler.simulador.api.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.witzler.simulador.api.model.entity.Distribuidoras;

public interface DistribuidorasRepository extends JpaRepository<Distribuidoras, Integer> {

}
