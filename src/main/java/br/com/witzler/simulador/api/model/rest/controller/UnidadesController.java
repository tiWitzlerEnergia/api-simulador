package br.com.witzler.simulador.api.model.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.witzler.simulador.api.dto.UnidadesDTO;
import br.com.witzler.simulador.api.model.entity.Unidades;
import br.com.witzler.simulador.api.model.repository.UnidadesRepository;

@RestController
@RequestMapping("/unidades/")
public class UnidadesController {
	
	private final UnidadesRepository repository;
	
	@Autowired
	public UnidadesController( UnidadesRepository repository ) {
		this.repository = repository;
	}
	
	@GetMapping("consulta/{id_empresa}")
	ArrayList<UnidadesDTO> listaUnidades( @PathVariable("id_empresa") Integer id_empresa ) {
		
		List<Unidades> unidades = repository.listarUnidadesPorEmpresa(id_empresa);
		
		ArrayList<UnidadesDTO> ConjuntoUnidades = new ArrayList<>();
		
		for(int i=0; i<unidades.size(); i++)
		{
			UnidadesDTO u = new UnidadesDTO();
			u.setId_unidade(unidades.get(i).getId_unidade());
			u.setId_empresa(unidades.get(i).getId_empresa());
			u.setNome_unidade(unidades.get(i).getNome_unidade());
			u.setDistribuidora(unidades.get(i).getDistribuidora());
			u.setTipo_tarifa(unidades.get(i).getTipo_tarifa());
			u.setDemanda_fora_ponta_unica_contratada(unidades.get(i).getDemanda_fora_ponta_unica_contratada());
			u.setDemanda_ponta_contratada(unidades.get(i).getDemanda_ponta_contratada());
			u.setConsumo_fora_ponta(unidades.get(i).getConsumo_fora_ponta());
			u.setConsumo_ponta(unidades.get(i).getConsumo_ponta());
			
			ConjuntoUnidades.add(u);
			
		}
		
		return ConjuntoUnidades;
		
	}
	
	
	@GetMapping("consulta/distribuidora/{id_unidade}")
	ArrayList<UnidadesDTO> listaDistribuidoraUnidade( @PathVariable("id_unidade") Integer id_unidade ) {
		
		List<Unidades> unidades = repository.listarUnidadesPorEmpresa(id_unidade);
		
		ArrayList<UnidadesDTO> ConjuntoUnidades = new ArrayList<>();
		
		for(int i=0; i<unidades.size(); i++)
		{
			UnidadesDTO u = new UnidadesDTO();
			u.setId_unidade(unidades.get(i).getId_unidade());
			u.setId_empresa(unidades.get(i).getId_empresa());
			u.setNome_unidade(unidades.get(i).getNome_unidade());
			u.setDistribuidora(unidades.get(i).getDistribuidora());
			u.setTipo_tarifa(unidades.get(i).getTipo_tarifa());
			u.setDemanda_fora_ponta_unica_contratada(unidades.get(i).getDemanda_fora_ponta_unica_contratada());
			u.setDemanda_ponta_contratada(unidades.get(i).getDemanda_ponta_contratada());
			u.setConsumo_fora_ponta(unidades.get(i).getConsumo_fora_ponta());
			u.setConsumo_ponta(unidades.get(i).getConsumo_ponta());
			
			ConjuntoUnidades.add(u);
			
		}
		
		return ConjuntoUnidades;
		
	}
	
	
}
