package br.com.witzler.simulador.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PrecoEnergiaDTO {
	
	private Integer id;
	private Double preco;
	private Integer ano;

}
