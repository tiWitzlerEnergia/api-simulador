package br.com.witzler.simulador.api.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.witzler.simulador.api.model.entity.Atuacao;

public interface AtuacaoRepository extends JpaRepository<Atuacao, Integer> {
	
	@Query(value="select * from atuacao", nativeQuery=true)
	List<Atuacao> listarTudoAtuacao();

}
