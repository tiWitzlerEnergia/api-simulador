package br.com.witzler.simulador.api.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.witzler.simulador.api.model.entity.Empresa;

public interface EmpresaRepository extends JpaRepository<Empresa, Integer>{
	
	@Query(value="select * from empresa where id_empresa = :id_empresa", nativeQuery=true)
	List<Empresa> listarEmpresa( @Param( "id_empresa" ) Integer id_empresa );
	
	
	// M�todo que retorna a quantidade de unidades que tem uma empresa espec�fica
	@Query(value="select * from empresa where id_empresa = :id_empresa", nativeQuery=true)
	List<Empresa> listarQtdeUnidadesEmpresa(@Param("id_empresa") Integer id_empresa);
	
	


}
