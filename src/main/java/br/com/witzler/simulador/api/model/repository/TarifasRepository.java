package br.com.witzler.simulador.api.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.witzler.simulador.api.model.entity.Tarifas;

public interface TarifasRepository extends JpaRepository<Tarifas, Integer>{
	
	// M�todo que retornas as tarifas de uma distribuidora espec�fica
	@Query(value="select * from tarifas where id_distribuidora = :id_distribuidora and tipo_tarifa = :tipo_tarifa", nativeQuery=true)
	List<Tarifas> listarTarifasPorDistribuidora( @Param( "id_distribuidora" ) Integer id_distribuidora, 
											     @Param( "tipo_tarifa" ) String tipo_tarifa );

}
