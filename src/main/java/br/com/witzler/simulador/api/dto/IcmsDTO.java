package br.com.witzler.simulador.api.dto;

import javax.persistence.Column;

import br.com.witzler.simulador.api.model.entity.Empresa;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IcmsDTO {
	
	private Integer id;
	private String estado;
	private double valor;

}
