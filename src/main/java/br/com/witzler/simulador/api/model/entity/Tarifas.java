package br.com.witzler.simulador.api.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table( name="tarifas")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Tarifas {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="id_distribuidora")
	private Integer id_distribuidora;
	
	@Column(name="subgrupo")
	private Integer subgrupo;
	
	@Column(name="demanda_ponta")
	private Double demanda_ponta;
	
	@Column(name="demanda_fora_ponta")
	private Double demanda_fora_ponta;
	
	@Column(name="consumo_ponta")
	private Double consumo_ponta;
	
	@Column(name="te_ponta")
	private Double te_ponta;
	
	@Column(name="te_fora_ponta")
	private Double te_fora_ponta;

	@Column(name="ultrapassagem_ponta")
	private Double ultrapassagem_ponta;
	
	@Column(name="ultrapassagem_fora_ponta")
	private Double ultrapassagem_fora_ponta;
	
	@Column(name="tipo_tarifa")
	private String tipo_tarifa;
	
	@Column(name="consumo_fora_ponta")
	private Double consumo_fora_ponta;
	
	@Column(name="reativo")
	private Double reativo;

}
