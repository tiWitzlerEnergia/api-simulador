package br.com.witzler.simulador.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DistribuidorasDTO {

	private Integer id_distribuidora;
	private String distribuidora;
	
}
