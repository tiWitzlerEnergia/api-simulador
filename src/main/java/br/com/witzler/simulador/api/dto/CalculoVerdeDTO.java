package br.com.witzler.simulador.api.dto;

import javax.persistence.Column;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CalculoVerdeDTO {

	private Integer id;
	private Integer id_unidade;
	private Integer id_empresa;
	private Integer ano;
	private Double mensal;
	private Double anual;
	
}
