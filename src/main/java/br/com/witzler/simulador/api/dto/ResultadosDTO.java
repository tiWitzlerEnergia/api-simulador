package br.com.witzler.simulador.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResultadosDTO {
	
	private Integer id;
	private Integer id_empresa;
	private Integer id_unidade;
	private String 	tipo_tarifa;
	private Integer ano;
	private Double  mensal;
	private Double  anual;
	private Double  porcentagem;

}
