package br.com.witzler.simulador.api.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table( name="empresa")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Empresa {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_empresa;
	
	@Column(name="nome_empresa")
	private String nome_empresa;
	
	@Column(name="cnpj")
	private String cnpj;
	
	@Column(name="email")
	private String email;
	
	@Column(name="nome_contato")
	private String nome_contato;
	
	@Column(name="telefone")
	private String telefone;
	
	@Column(name="celular")
	private String celular;
	
	@Column(name="area_atuacao")
	private Integer area_atuacao;
	
	@Column(name="especialidade")
	private Integer especialidade;
	
	@Column(name="numero_unidades")
	private Integer numero_unidades;

}
