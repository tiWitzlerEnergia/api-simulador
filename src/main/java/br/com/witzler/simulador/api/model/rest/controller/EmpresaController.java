package br.com.witzler.simulador.api.model.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.witzler.simulador.api.dto.EmpresaDTO;
import br.com.witzler.simulador.api.model.entity.Empresa;
import br.com.witzler.simulador.api.model.repository.EmpresaRepository;

@RestController
@RequestMapping("/empresa/")
public class EmpresaController {
	
	private final EmpresaRepository repository;
	
	@Autowired
	public EmpresaController( EmpresaRepository repository ) {
		this.repository = repository;
	}
	
	@GetMapping("consulta/unidades/{id_empresa}")
	ArrayList<EmpresaDTO> listaUnidadesEmpresa( @PathVariable("id_empresa") Integer id_empresa ) {
		
		List<Empresa> empresa = repository.listarQtdeUnidadesEmpresa(id_empresa);
		
		ArrayList<EmpresaDTO> ConjuntoEmpresa = new ArrayList<>();
		
		for(int i=0; i<empresa.size(); i++)
		{
			EmpresaDTO e = new EmpresaDTO();
			e.setId_empresa(empresa.get(i).getId_empresa());
			e.setNome_empresa(empresa.get(i).getNome_empresa());
			e.setNumero_unidades(empresa.get(i).getNumero_unidades());
			
			ConjuntoEmpresa.add(e);
			
		}
		
		return ConjuntoEmpresa;
		
	}
	
	@GetMapping("consulta/{id_empresa}")
	ArrayList<EmpresaDTO> listaEmpresa( @PathVariable( "id_empresa" ) Integer id_empresa ) {
		
		List<Empresa> empresa = repository.listarEmpresa( id_empresa );
		
		ArrayList<EmpresaDTO> ConjuntoEmpresa = new ArrayList<>();
		
		for( int i=0; i<empresa.size(); i++ )
		{
			EmpresaDTO e = new EmpresaDTO();
			e.setId_empresa(empresa.get(i).getId_empresa());
			
			ConjuntoEmpresa.add(e);
			
		}
		
		return ConjuntoEmpresa;
		
	}
	
}
