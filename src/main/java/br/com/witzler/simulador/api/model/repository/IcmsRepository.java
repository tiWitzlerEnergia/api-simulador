package br.com.witzler.simulador.api.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.witzler.simulador.api.model.entity.Icms;
import br.com.witzler.simulador.api.model.entity.Tarifas;

public interface IcmsRepository extends JpaRepository<Icms, Integer>{
	
	// Listando valor do icms por id do estado
	@Query(value="select * from icms where id = :id", nativeQuery=true)
	List<Icms> listarIcmsPorIdEstado( @Param( "id" ) Integer id );

}
