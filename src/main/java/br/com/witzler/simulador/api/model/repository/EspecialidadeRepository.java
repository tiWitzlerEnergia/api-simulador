package br.com.witzler.simulador.api.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.witzler.simulador.api.model.entity.Especialidade;

public interface EspecialidadeRepository extends JpaRepository<Especialidade, Integer> {
	
	@Query(value="select * from especialidade", nativeQuery=true)
	List<Especialidade> listarTudoEspecialidade();

}
