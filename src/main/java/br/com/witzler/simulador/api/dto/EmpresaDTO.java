package br.com.witzler.simulador.api.dto;

import javax.persistence.Column;

import br.com.witzler.simulador.api.model.entity.Empresa;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmpresaDTO {
	
	private Integer id_empresa;
	private String nome_empresa;
	private String cnpj;
	private String email;
	private String nome_contato;
	private String telefone;
	private String celular;
	private Integer area_atuacao;
	private Integer especialidade;
	private Integer numero_unidades;

}
