package br.com.witzler.simulador.api.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table( name="distribuidoras")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Distribuidoras {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_distribuidora;
	
	@Column(name="distribuidora")
	private String distribuidora;

}
