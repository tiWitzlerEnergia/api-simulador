package br.com.witzler.simulador.api.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.witzler.simulador.api.model.entity.PrecoEnergia;

public interface PrecoEnergiaRepository extends JpaRepository<PrecoEnergia, Integer> {
	
	@Query(value="select * from precoEnergia where ano = :ano", nativeQuery=true)
	List<PrecoEnergia> listarPrecoEnergia( @Param( "ano") Integer ano);

}
