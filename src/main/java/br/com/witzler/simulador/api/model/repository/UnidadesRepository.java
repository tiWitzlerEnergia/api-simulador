package br.com.witzler.simulador.api.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.witzler.simulador.api.model.entity.Unidades;

public interface UnidadesRepository extends JpaRepository<Unidades, Integer> {
	
	// M�todo que retorna as unidades espec�ficas de uma empresa espec�fica
	@Query(value="select * from unidades where id_empresa = :id_empresa", nativeQuery=true)
	List<Unidades> listarUnidadesPorEmpresa(@Param("id_empresa") Integer id_empresa);
	
	@Query(value="select * from unidades where id_unidade = :id_unidade", nativeQuery=true)
	List<Unidades> listarDistribuidoraUnidade(@Param("id_unidade") Integer id_unidade);
	
	
	
}
